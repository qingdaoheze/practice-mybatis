package com.demo.jdbc.entity;

import lombok.Data;

import java.util.Date;

/**
 * #
 * # Add some message
 * <p>
 * 2019/5/20
 * #
 *
 * @author qd-hz
 */
@Data
public class Person {
    private Integer id;
    private String name;
    private Integer age;
    private Date createTime;
    private Date updateTime;
}
