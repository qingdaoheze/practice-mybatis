package com.demo.jdbc.raw;

import com.demo.jdbc.entity.Person;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * #
 * # Add some message
 * <p>
 * 2019/5/20
 * #
 *
 * @author qd-hz
 */
@Slf4j
public class RawJdbc {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.info("驱动类未找到", e);
            return;
        }
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            // 1、建立连接
            String connectionString = "jdbc:mysql://localhost:3306/java_demo?useUnicode=true&characterEncoding=utf-8&useSSL=false";
            connection = DriverManager.getConnection(connectionString, "root", "123456");

            // 2、开启事务
            connection.setAutoCommit(false);

            // 3、插入数据
            String insertSql = "INSERT INTO `java_demo`.`person` " +
                    "(`id`, `name`, `age`, `create_time`, `update_time`) " +
                    "VALUES " +
                    "('2', 'Wang', '16', '2019-05-20 09:55:21', '2019-05-21 09:55:30')";
            statement = connection.createStatement();
            int insertCount = statement.executeUpdate(insertSql);
            log.info("insert result: {}", insertCount);

            /// 获取自动生成的ID
            // ResultSet generatedKeys = statement.getGeneratedKeys();

            /// 另一种方式
//            PreparedStatement preparedStatement = connection.prepareStatement("");
//            preparedStatement.execute();


            // 4、更新数据
            String updateSql = "UPDATE `java_demo`.`person` " +
                    "SET `age`='30' " +
                    "WHERE `id`='2'";
            int updateCount = statement.executeUpdate(updateSql);
            log.info("update result: {}", updateCount);

            // 5、查询
            String queryString = "SELECT * FROM java_demo.person";
            resultSet = statement.executeQuery(queryString);
            List<Person> resultList = new ArrayList<>();
            while (resultSet.next()) {
                Person person = new Person();

                person.setId(resultSet.getInt("id"));
                person.setName(resultSet.getString("name"));
                person.setAge(resultSet.getInt("age"));
                person.setCreateTime(resultSet.getTimestamp("create_time"));
                person.setUpdateTime(resultSet.getTimestamp("update_time"));

                resultList.add(person);
            }

            for (Person person : resultList) {
                log.info("{}", person);
            }

            // 6、提交
            connection.rollback();

        } catch (SQLException e) {
            log.error("操作失败", e);

            // 7、回滚
            try {
                connection.rollback();
            } catch (SQLException e1) {
                log.error("回滚失败.", e1);
            }
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    log.error("ignore", e);
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    log.error("ignore", e);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.error("ignore", e);
                }
            }
        }
    }
}
