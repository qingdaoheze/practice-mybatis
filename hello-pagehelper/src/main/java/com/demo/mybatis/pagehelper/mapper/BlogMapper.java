package com.demo.mybatis.pagehelper.mapper;

import com.demo.mybatis.pagehelper.entity.Blog;
import com.demo.mybatis.pagehelper.handler.BlogTypeEnumHandler;
import org.apache.ibatis.annotations.*;

import java.util.List;

@CacheNamespace()
public interface BlogMapper {

	/**
	 *	查询
	 * @return
	 */
	@Select("select * from blog")
	@Results(id="blog", value = {
			@Result(property = "id", column = "id", id = true),
			@Result(property = "title", column = "title"),
			@Result(property = "type", column = "type", typeHandler = BlogTypeEnumHandler.class)
	})
	List<Blog> selectBlog();

}
