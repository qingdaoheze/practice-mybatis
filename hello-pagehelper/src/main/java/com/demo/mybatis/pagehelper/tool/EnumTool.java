package com.demo.mybatis.pagehelper.tool;

/**
 *
 * 枚举工具类
 *
 * @author qd-hz
 */
public class EnumTool {
    public static <T extends Enum<T>> T parseEnum(Class<T> enumClass, int ordinal) {
        T[] enumConstants = enumClass.getEnumConstants();
        int length = enumConstants.length;
        if (ordinal < 0 || ordinal >= length) {
            throw new IndexOutOfBoundsException("invalid ordinal");
        }
        return enumConstants[ordinal];
    }
}
