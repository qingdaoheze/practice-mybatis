package com.demo.mybatis.pagehelper;

import com.demo.mybatis.pagehelper.entity.Blog;
import com.demo.mybatis.pagehelper.mapper.BlogMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/4
 * #
 *
 * @author qd-hz
 */
@Slf4j
public class HelloPageHelper {

    public static void main(String[] args) {
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        try {
            InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
            SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
            SqlSession sqlSession = sqlSessionFactory.openSession();
            BlogMapper blogMapper = sqlSession.getMapper(BlogMapper.class);

            PageHelper.startPage(3, 1);
            List<Blog> blogList = blogMapper.selectBlog();

            PageInfo<Blog> pageInfo = new PageInfo<>(blogList, 3);
            log.info(pageInfo.toString());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
