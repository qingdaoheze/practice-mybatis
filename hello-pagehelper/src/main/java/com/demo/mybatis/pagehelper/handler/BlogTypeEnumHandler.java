package com.demo.mybatis.pagehelper.handler;

import com.demo.mybatis.pagehelper.entity.Blog;
import com.demo.mybatis.pagehelper.enums.BlogTypeEnum;
import com.demo.mybatis.pagehelper.tool.EnumTool;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/4
 * #
 *
 * @author qd-hz
 */
public class BlogTypeEnumHandler extends BaseTypeHandler<BlogTypeEnum> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, BlogTypeEnum parameter, JdbcType jdbcType) throws SQLException {
        // 顺序
        ps.setInt(i, parameter.getId());
    }

    @Override
    public BlogTypeEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        int ordinal = rs.getInt(columnName);
        if (rs.wasNull()) {
            return null;
        }
        BlogTypeEnum blogTypeEnum = EnumTool.parseEnum(BlogTypeEnum.class, ordinal);

        return blogTypeEnum;
    }

    @Override
    public BlogTypeEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        int ordinal = rs.getInt(columnIndex);
        if (rs.wasNull()) {
            return null;
        }
        BlogTypeEnum blogTypeEnum = EnumTool.parseEnum(BlogTypeEnum.class, ordinal);

        return blogTypeEnum;
    }

    @Override
    public BlogTypeEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        int ordinal = cs.getInt(columnIndex);
        if (cs.wasNull()) {
            return null;
        }
        BlogTypeEnum blogTypeEnum = EnumTool.parseEnum(BlogTypeEnum.class, ordinal);

        return blogTypeEnum;
    }
}
