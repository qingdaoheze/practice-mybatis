package com.demo.mybatis.pagehelper.entity;

import com.demo.mybatis.pagehelper.enums.BlogTypeEnum;
import lombok.Data;

import java.io.Serializable;

@Data
public class Blog implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private long id;
	private String title;
	private String content;
	private BlogTypeEnum type;
}
