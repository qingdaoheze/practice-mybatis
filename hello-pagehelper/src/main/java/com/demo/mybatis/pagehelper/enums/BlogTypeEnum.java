package com.demo.mybatis.pagehelper.enums;

import lombok.Getter;
import lombok.ToString;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/4
 * #
 *
 * @author qd-hz
 */
@Getter
@ToString
public enum BlogTypeEnum {
    RED(0), BLUE(1), GREEN(2), YELLOW(3);
    private int id;
    private BlogTypeEnum(int id) {
        this.id = id;
    }
}
