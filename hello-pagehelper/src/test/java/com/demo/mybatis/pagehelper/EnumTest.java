package com.demo.mybatis.pagehelper;

import com.demo.mybatis.pagehelper.enums.BlogTypeEnum;
import com.demo.mybatis.pagehelper.tool.EnumTool;
import lombok.extern.slf4j.Slf4j;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/4
 * #
 *
 * @author qd-hz
 */
@Slf4j
public class EnumTest {
    public static void main(String[] args) {
        log.info("RED(0) is {}", BlogTypeEnum.RED.ordinal());
        log.info("BLUE(1) is {}", BlogTypeEnum.BLUE.ordinal());
        log.info("GREEN(2) is {}", BlogTypeEnum.GREEN.ordinal());
        log.info("Ordinal 0 is {}", EnumTool.parseEnum(BlogTypeEnum.class, 0));
        log.info("Ordinal 1 is {}", EnumTool.parseEnum(BlogTypeEnum.class, 1));
        log.info("Ordinal 2 is {}", EnumTool.parseEnum(BlogTypeEnum.class, 2));
        log.info("Ordinal 3 is {}", EnumTool.parseEnum(BlogTypeEnum.class, 3));
    }
}
