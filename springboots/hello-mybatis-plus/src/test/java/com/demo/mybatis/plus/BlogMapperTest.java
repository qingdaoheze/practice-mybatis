package com.demo.mybatis.plus;

import com.demo.mybatis.plus.entity.Blog;
import com.demo.mybatis.plus.entity.BlogEntity;
import com.demo.mybatis.plus.mapper.BlogMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/5
 * #
 *
 * @author qd-hz
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BlogMapperTest {

    @Autowired
    private BlogMapper blogMapper;

    @Test
    public void test() {
        List<BlogEntity> blogList = blogMapper.selectList(null);
        for (BlogEntity blog : blogList) {
            log.info(blog.toString());
        }

    }
}
