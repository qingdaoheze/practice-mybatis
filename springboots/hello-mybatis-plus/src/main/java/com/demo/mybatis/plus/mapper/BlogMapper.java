package com.demo.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.mybatis.plus.entity.BlogEntity;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Mybatis-Plus-Generator
 * @since 2019-06-06
 */
public interface BlogMapper extends BaseMapper<BlogEntity> {

}
