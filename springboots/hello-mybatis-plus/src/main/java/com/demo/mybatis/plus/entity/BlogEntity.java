package com.demo.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author Mybatis-Plus-Generator
 * @since 2019-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("blog")
public class BlogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "title", fill = FieldFill.UPDATE)
    private String title;

    @TableField(value = "content", fill = FieldFill.INSERT)
    private String content;

    @TableField(value = "type", fill = FieldFill.INSERT_UPDATE)
    private Integer type;

    @TableField("version")
    private Long version;

    @TableField(value = "active", fill = FieldFill.DEFAULT)
    @TableLogic
    private Boolean active;


}
