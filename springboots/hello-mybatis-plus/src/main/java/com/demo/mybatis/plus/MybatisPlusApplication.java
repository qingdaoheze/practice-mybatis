package com.demo.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/5
 * #
 *
 * @author qd-hz
 */
@SpringBootApplication
@MapperScan("com.demo.mybatis.plus.mapper")
public class MybatisPlusApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApplication.class, args);
    }
}
