package com.demo.mybatis.springboot.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 多数据源配置
 *
 * @author qd-hz
 */
@Configuration
@Slf4j
public class DataSourceConfig {
    @Autowired
    @Qualifier("primaryDataSource")
    DataSource myDataSource;

    @Bean
    @ConfigurationProperties("spring.datasource.master")
    public DataSource master1() {
        log.info("*********** master1 construct");
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.slave1")
    public DataSource slave1() {
        log.info("*********** slave1 construct");
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.slave2")
    public DataSource slave2() {
        log.info("*********** slave2 construct");
        return DataSourceBuilder.create().build();
    }

    @Bean
    public DataSource primaryDataSource(@Qualifier("master1") DataSource master1,
                                        @Qualifier("slave1") DataSource slave1,
                                        @Qualifier("slave2") DataSource slave2) {
        log.info("*********** primaryDataSource construct");
        Map<Object, Object> dataSourceMap = new HashMap<>();
        dataSourceMap.put(DataSourceTypeEnum.MASTER, master1);
        dataSourceMap.put(DataSourceTypeEnum.SLAVE1, slave1);
        dataSourceMap.put(DataSourceTypeEnum.SLAVE2, slave2);

        MyRoutingDataSource routingDataSource = new MyRoutingDataSource();
        routingDataSource.setDefaultTargetDataSource(master1);
        routingDataSource.setTargetDataSources(dataSourceMap);
        return routingDataSource;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(myDataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:com/demo/mybatis/**/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    @Bean
    public PlatformTransactionManager platformTransactionManager() {
        return new DataSourceTransactionManager(myDataSource);
    }
}
