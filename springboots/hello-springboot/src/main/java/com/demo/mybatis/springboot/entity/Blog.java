package com.demo.mybatis.springboot.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Blog implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private long id;
	private String title;
	private String content;
	private int type;
}
