package com.demo.mybatis.springboot.mapper;

import com.demo.mybatis.springboot.annotation.Master;
import com.demo.mybatis.springboot.entity.Blog;

import java.util.List;

public interface BlogMapper {

    /**
     * 主库查询
     *
     * @return
     */
    List<Blog> selectBlog();

    /**
     * 分库查询
     *
     * @return
     */
    List<Blog> selectBlog1();

    /**
     * 分库查询
     *
     * @return
     */
    List<Blog> insertBlog();

}
