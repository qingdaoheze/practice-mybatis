package com.demo.mybatis.springboot.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 自动切换数据源
 *
 * @author qd-hz
 */
@Aspect
@Order(1)
@Component
@Slf4j
public class DataSourceAop {

    @Pointcut("@annotation(com.demo.mybatis.springboot.annotation.Slave) " +
            "|| (execution(!@com.demo.mybatis.springboot.annotation.Master * com.demo.mybatis.springboot..*Impl.select*(..))) ")
    public void readPointCut() {
    }

    @Pointcut("@annotation(com.demo.mybatis.springboot.annotation.Master) " +
            "|| execution(!@com.demo.mybatis.springboot.annotation.Slave * com.demo.mybatis.springboot..*Impl.insert*()) " +
            "|| execution(!@com.demo.mybatis.springboot.annotation.Slave * com.demo.mybatis.springboot..*Impl.update*()) " +
            "|| execution(!@com.demo.mybatis.springboot.annotation.Slave * com.demo.mybatis.springboot..*Impl.delete*()) ")
    public void writerPointCut() {

    }

    @Before("readPointCut()")
    public void read(JoinPoint jp) {
        String methodName = jp.getSignature().getName();
        log.info("read method {}", methodName);
        DataSourceContextHolder.slave();
    }

    @Before("writerPointCut()")
    public void write(JoinPoint jp) {
        String methodName = jp.getSignature().getName();
        log.info("write method {}", methodName);
        DataSourceContextHolder.master();
    }

    /**
     * 另一种写法：if...else...  判断哪些需要读从数据库，其余的走主数据库
     */
//    @Before("execution(* com.cjs.example.service.impl.*.*(..))")
//    public void before(JoinPoint jp) {
//        String methodName = jp.getSignature().getName();
//
//        if (StringUtils.startsWithAny(methodName, "get", "select", "find")) {
//            DBContextHolder.slave();
//        }else {
//            DBContextHolder.master();
//        }
//    }
}
