package com.demo.mybatis.springboot.annotation;

/**
 * 数据库主库
 *
 * @author qd-hz
 */
public @interface Master {
}
