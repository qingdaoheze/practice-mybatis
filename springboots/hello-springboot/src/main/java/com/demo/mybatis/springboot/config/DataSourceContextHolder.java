package com.demo.mybatis.springboot.config;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 数据源上下文
 *
 * @author qd-hz
 */
@Slf4j
public class DataSourceContextHolder {

    private static ThreadLocal<DataSourceTypeEnum> contextHolder = new ThreadLocal<DataSourceTypeEnum>();
    private static AtomicInteger counter = new AtomicInteger(0);

    public static void set(DataSourceTypeEnum dataSourceType) {
        contextHolder.set(dataSourceType);
    }

    public static DataSourceTypeEnum get() {
        return contextHolder.get();
    }

    /** 设置为主库 */
    public static void master() {
        set(DataSourceTypeEnum.MASTER);
        log.info("切换到MASTER");
    }

    /** 设置为从库，在多个从库间轮询 */
    public static void slave() {
        // 轮询
        int index = counter.getAndIncrement() % 2;

        if (counter.get() > 9999) {
            counter.set(0);
        }

        if (index == 0) {
            set(DataSourceTypeEnum.SLAVE1);
            log.info("切换到SLAVE1");
        } else {
            set(DataSourceTypeEnum.SLAVE2);
            log.info("切换到SLAVE2");
        }
    }

}
