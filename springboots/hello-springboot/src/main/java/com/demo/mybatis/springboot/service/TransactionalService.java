package com.demo.mybatis.springboot.service;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/25
 * #
 *
 * @author qd-hz
 */
public interface TransactionalService {
    /**
     * 默认规则
     */
    void selectBlog();

    /**
     * 注解控制
     */
    void selectBlog1();

    /**
     * 默认规则
     */
    void selectBlogWithTrans();

    /**
     * 注解控制
     */
    void selectBlog1WithTrans();

    /**
     * 默认规则
     */
    void insertBlog();

    /**
     * 复合操作
     */
    void multiOperBlog();

    /**
     * 带事务的复合操作
     */
    void multiOperWithTransaction();
}
