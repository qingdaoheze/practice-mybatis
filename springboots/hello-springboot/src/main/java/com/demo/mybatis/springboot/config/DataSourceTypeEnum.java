package com.demo.mybatis.springboot.config;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/21
 * #
 *
 * @author qd-hz
 */
public enum DataSourceTypeEnum {
    /** 主数据源 */
    MASTER,
    /** 从数据源1 */
    SLAVE1,
    /** 从数据源2 */
    SLAVE2;
}
