package com.demo.mybatis.springboot.mapper;

import com.demo.mybatis.springboot.entity.Blog;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ScannedBlogMapper {

	/**
	 *	查询
	 * @return
	 */
	@Select("select * from blog")
	@Results(id="blog", value = {
			@Result(property = "id", column = "id", id = true),
			@Result(property = "title", column = "title"),
			@Result(property = "type", column = "type")
	})
	List<Blog> selectBlog();

}
