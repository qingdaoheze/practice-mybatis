package com.demo.mybatis.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 *
 * @author qd-hz
 */
@MapperScan(basePackages = "com.demo.mybatis.springboot.mapper")
@SpringBootApplication
public class HelloSpringBootApplicaiton {
    public static void main(String[] args) {
        SpringApplication.run(HelloSpringBootApplicaiton.class, args);

    }
}
