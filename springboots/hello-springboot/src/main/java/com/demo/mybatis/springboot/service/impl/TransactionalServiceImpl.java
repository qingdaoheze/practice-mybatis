package com.demo.mybatis.springboot.service.impl;

import com.demo.mybatis.springboot.annotation.Master;
import com.demo.mybatis.springboot.annotation.Slave;
import com.demo.mybatis.springboot.entity.Blog;
import com.demo.mybatis.springboot.mapper.BlogMapper;
import com.demo.mybatis.springboot.service.TransactionalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/25
 * #
 *
 * @author qd-hz
 */
@Service
@Slf4j
public class TransactionalServiceImpl implements TransactionalService {

    @Autowired
    BlogMapper blogMapper;

    @Autowired
    TransactionalService transactionalService;

    @Override
    public void selectBlog() {
        List<Blog> blogs = blogMapper.selectBlog();
        printBlog(blogs);
    }

    @Override
    @Master
    public void selectBlog1() {
        selectBlog();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void selectBlogWithTrans() {
        selectBlog();
    }

    @Override
    @Master
    @Transactional(propagation = Propagation.NOT_SUPPORTED ,rollbackFor = Exception.class)
    public void selectBlog1WithTrans() {
        selectBlog();
    }

    @Override
    public void insertBlog() {
        List<Blog> blogs = blogMapper.selectBlog();
        printBlog(blogs);
    }

    @Override
    public void multiOperBlog() {
        log.info("--------------------");
        transactionalService.selectBlog();
        transactionalService.selectBlog1();
        log.info("====================");
    }

    @Override
    @Transactional
    @Slave
    public void multiOperWithTransaction() {
        log.info("--------------------");
        transactionalService.selectBlogWithTrans();
        transactionalService.selectBlog1WithTrans();
        log.info("====================");
    }

    void printBlog(List<Blog> blogList) {
        if (blogList == null || blogList.isEmpty()) {
            log.info("blogList is empty.");
        } else {
            log.info(blogList.get(0).toString());
        }
    }
}
