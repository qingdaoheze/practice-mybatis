package com.demo.mybatis.springboot;

import com.demo.mybatis.springboot.service.TransactionalService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/25
 * #
 *
 * @author qd-hz
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TransactionalServiceTest {
    @Autowired
    TransactionalService transactionalService;

    @Test
    public void selectBlog() {
        transactionalService.selectBlog();
    }

    @Test
    public void selectBlog1() {
        transactionalService.selectBlog1();
    }

    @Test
    public void selectBlogWithTrans() {
        transactionalService.selectBlogWithTrans();
    }

    @Test
    public void selectBlog1WithTrans() {
        transactionalService.selectBlog1WithTrans();
    }

    @Test
    public void insertBlog() {
        transactionalService.insertBlog();
    }

    @Test
    public void multiOperBlog() {
        transactionalService.multiOperBlog();
    }

    @Test
    public void multiOperWithTransaction() {
        transactionalService.multiOperWithTransaction();
    }
}
