package com.demo.mybatis.springboot;

import com.demo.mybatis.springboot.entity.Blog;
import com.demo.mybatis.springboot.mapper.BlogMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/4
 * #
 *
 * @author qd-hz
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelloSpringBootApplicaiton.class)
@Slf4j
public class PageHelperTest {
    @Autowired
    private BlogMapper blogMapper;

    @Test
    public void test() {
        // 设置分页信息
        PageHelper.startPage(1, 3);

        List<Blog> blogList = blogMapper.selectBlog();

        // 包装查询结果
        PageInfo pageInfo = new PageInfo(blogList);

        log.info(pageInfo.toString());
    }
}
