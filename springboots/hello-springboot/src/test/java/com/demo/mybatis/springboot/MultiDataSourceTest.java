package com.demo.mybatis.springboot;

import com.demo.mybatis.springboot.entity.Blog;
import com.demo.mybatis.springboot.mapper.BlogMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/21
 * #
 *
 * @author qd-hz
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MultiDataSourceTest {
    @Autowired
    private BlogMapper blogMapper;

    /**
     * 不使用事务时，每次都重新获取一个连接
     */
    @Test
    public void test() {
        common();
    }

    /**
     * 使用事务时，启动事务时就获取了连接
     */
    @Test
    @Transactional
    public void selectTest() {
        common();
    }

    public void common() {

        List<Blog> blogs = blogMapper.selectBlog();

        for (int i = 0; i < 1; i++) {

            if (blogs != null && !blogs.isEmpty()) {
                log.info("selectBlog {}", blogs.get(0).toString());
            }

            blogs = blogMapper.selectBlog1();
            if (blogs != null && !blogs.isEmpty()) {
                log.info("selectBlog1 {}", blogs.get(0).toString());
            }

            blogs = blogMapper.insertBlog();
            if (blogs != null && !blogs.isEmpty()) {
                log.info("addBlog {}", blogs.get(0).toString());
            }
        }
    }
}
