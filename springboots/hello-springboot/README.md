# 添加依赖
```xml

        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>1.3.2</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
```
# 添加application.properties
```properties
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/java_demo?userUnicode=true&characterEncoding=utf-8&useSSL=false
spring.datasource.username=root
spring.datasource.password=123456

# 可以使用ant风格的统配符号，多个路径之间用逗号分割
# mybatis.mapper-locations=classpath*:com/demo/mybatis/**/*.xml,classpath*:xml/*.xml
mybatis.mapper-locations=classpath:xml/*.xml
```

# 启动类添加MapperScann
```java
@MapperScan(basePackages = "com.demo.mybatis.springboot.mapper")
@SpringBootApplication
public class HelloSpringBootApplicaiton {
    public static void main(String[] args) {
        SpringApplication.run(HelloSpringBootApplicaiton.class, args);
    }
}
```
*MapperScan注解是必须添加的，否则不会在spring容器中注册Mapper。但会在mybatis的容器中注册，即可以使用sqlSession的getMapper方法获取到。*

# pagehelper
## 添加依赖
```xml
        <dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper-spring-boot-starter</artifactId>
            <version>1.2.11</version>
        </dependency>
```
## 添加配置
```properties
pagehelper.dialect=msyql
```


## 多数据源配置
`com.demo.mybatis.springboot.config.DataSourceConfig`
