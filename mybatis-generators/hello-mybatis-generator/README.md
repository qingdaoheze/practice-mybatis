# mybatis-generator初体验
http://www.mybatis.org/generator/

# 操作步骤
1. 导入依赖
在maven的pom文件中导入插件`mybatis-generator-maven-plugin`

2. 添加配置文件
`src/main/resources/mybatis-generator.xml`

3. 运行插件
`mvn org.mybatis.generator:mybatis-generator-maven-plugin:1.3.5:generate`

# 小技巧
## 关闭Example相关元素的生成
在table元素中添加如下属性 
enableCountByExample="false" enableUpdateByExample="false"
enableDeleteByExample="false" enableSelectByExample="false"
selectByExampleQueryId="false" 如代码：
```xml
<table tableName="blog" domainObjectName="Blog"
    enableCountByExample="false" enableUpdateByExample="false"
    enableDeleteByExample="false" enableSelectByExample="false"
    selectByExampleQueryId="false">
    <property name="useActualColumnNames" value="false" ></property>
    <generatedKey column="ID" sqlStatement="mysql" identity="true" ></generatedKey>
</table>
```
