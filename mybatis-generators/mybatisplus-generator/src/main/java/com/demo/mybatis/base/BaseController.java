package com.demo.mybatis.base;

/**
 * Base Controller
 *
 * @author qd-hz
 */
public class BaseController {
    public String getControllerName() {
        return getClass().getSimpleName();
    }
}
