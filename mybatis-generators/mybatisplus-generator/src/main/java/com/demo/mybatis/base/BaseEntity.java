package com.demo.mybatis.base;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * #
 * # Add some message
 * <p>
 * 2019/6/6
 * #
 *
 * @author qd-hz
 */
@Data
@Accessors(chain = true)
public class BaseEntity {
    /** 主键 */
    private Long id;
    /** 乐观锁 */
    private Long version;
}
