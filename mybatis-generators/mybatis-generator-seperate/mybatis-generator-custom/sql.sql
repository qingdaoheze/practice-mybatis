﻿CREATE TABLE `t_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `sex` bit(1) DEFAULT NULL,
  `sex_male` varchar(30) DEFAULT NULL,
  `sex_female1` varchar(30) DEFAULT NULL,
  `sex_female2` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO `t_users` (`id`, `user_id`, `group_id`, `sex`, `sex_male`, `sex_female1`, `sex_female2`)
VALUES
	(1, 1, 8, 00000000, NULL, 'fe1', 'fe2'),
	(2, 2, 8, 00000000, 'm1', NULL, NULL),
	(3, 3, 8, 00000000, 'm2', NULL, NULL);


CREATE TABLE `t_posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO `t_posts` (`id`, `user_id`, `title`)
VALUES
	(1, 1, 'a'),
	(2, 1, 'b');