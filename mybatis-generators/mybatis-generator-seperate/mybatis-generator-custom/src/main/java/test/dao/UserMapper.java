package test.dao;

import lfg.mybatis.base.GenericMapper;
import test.model.ext.UserExampleExt;
import test.model.ext.UserExt;

public interface UserMapper extends GenericMapper<UserExt, UserExampleExt, Integer> {
}