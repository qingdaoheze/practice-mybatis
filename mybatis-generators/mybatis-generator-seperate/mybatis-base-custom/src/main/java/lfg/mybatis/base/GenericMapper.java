/**
 * 
 */
package lfg.mybatis.base;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 *
 * T  表实体 <br/>
 * TE example实体 <br/>
 * PK 主键实体 <br/>
 * @author lfg
 *
 */
public interface GenericMapper <T, TE, PK> {
    long countByExample(TE example);

    int deleteByExample(TE example);

    int deleteByPrimaryKey(PK id);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByExample(TE example);

    T selectByPrimaryKey(PK id);

    int updateByExampleSelective(@Param("record") T record, @Param("example") TE example);

    int updateByExample(T record, TE example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

}