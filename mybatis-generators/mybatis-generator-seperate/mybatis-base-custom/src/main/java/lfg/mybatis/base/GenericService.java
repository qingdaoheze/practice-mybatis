/**
 *@date 2017年11月8日 下午3:33:09 
 */
package lfg.mybatis.base;

import java.util.List;

/**
 * @author lfg
 *
 */
public interface GenericService <T, TE, PK> {
    long countByExample(TE example);

    int deleteByExample(TE example);

    int deleteByPrimaryKey(PK id);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByExample(TE example);

    T selectByPrimaryKey(PK id);

    int updateByExampleSelective(T record, TE example);

    int updateByExample(T record, TE example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

    List<T> queryAll();
}
