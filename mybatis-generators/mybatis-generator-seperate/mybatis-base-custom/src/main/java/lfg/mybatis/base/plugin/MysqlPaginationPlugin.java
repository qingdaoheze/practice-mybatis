/**
 *@date 2017年11月9日 上午9:31:24 
 */
package lfg.mybatis.base.plugin;

import java.util.List;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

/**
 * Add pagination elements.
 * 
 * @author lfg
 *
 */
public class MysqlPaginationPlugin  extends PluginAdapter {

	@Override
	public boolean validate(List<String> warnings) {
		return true;
	}

	@Override
	public boolean modelExampleClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		FullyQualifiedJavaType rowBoundsType = new FullyQualifiedJavaType("org.apache.ibatis.session.RowBounds");
		// add import 
		topLevelClass.addImportedType(rowBoundsType);
		// add field
		Field rowBoundsField = new Field("rowBounds", rowBoundsType);
		rowBoundsField.setVisibility(JavaVisibility.PUBLIC);
		topLevelClass.addField(rowBoundsField);
		// get method
		Method getMethod = new Method("getRowBounds");
		getMethod.setVisibility(JavaVisibility.PUBLIC);
		getMethod.setReturnType(rowBoundsType);
		getMethod.addBodyLine("return rowBounds;");
		topLevelClass.addMethod(getMethod);
		// set method
		Method setMethod = new Method("setRowBounds");
		setMethod.setVisibility(JavaVisibility.PUBLIC);
		setMethod.addParameter(new Parameter(rowBoundsType, "rowBounds"));
		setMethod.addBodyLine("if (rowBounds != null) {");
		setMethod.addBodyLine("this.rowBounds = rowBounds;");
		setMethod.addBodyLine("}");
		return super.modelExampleClassGenerated(topLevelClass, introspectedTable);
	}
	
	@Override
	public boolean sqlMapSelectByExampleWithBLOBsElementGenerated(XmlElement element,
			IntrospectedTable introspectedTable) {
		addLimitClause(element);
		return super.sqlMapSelectByExampleWithBLOBsElementGenerated(element, introspectedTable);
	}

	@Override
	public boolean sqlMapSelectByExampleWithoutBLOBsElementGenerated(XmlElement element,
			IntrospectedTable introspectedTable) {
		addLimitClause(element);
		return super.sqlMapSelectByExampleWithoutBLOBsElementGenerated(element, introspectedTable);
	}
	
	private void addLimitClause(XmlElement element) {
		XmlElement paginateElement = new XmlElement("if");
		paginateElement.addAttribute(new Attribute("test", "rowBounds != null"));
		String limitClause = "limit ${rowBounds.getOffset()} ${rowBounds.getLimit()}";
		paginateElement.addElement(new TextElement(limitClause));
		element.addElement(paginateElement);
	}
	
}