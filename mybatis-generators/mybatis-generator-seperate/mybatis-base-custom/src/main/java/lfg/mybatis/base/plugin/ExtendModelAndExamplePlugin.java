/**
 *@date 2017年11月9日 上午10:01:02 
 */
package lfg.mybatis.base.plugin;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.JavaFormatter;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.XmlFormatter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.XmlConstants;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.ModelType;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.mybatis.generator.exception.ShellException;
import org.mybatis.generator.internal.DefaultShellCallback;

import lfg.mybatis.base.GenericMapper;
import lfg.mybatis.base.GenericMapperWithBlobs;

/**
 * Save the default model type as the base type, and add additional ext type for
 * developers to customize actions; The same to example type.
 * 
 * There are two properties: {@link #PROPERTY_NAME_TARGET_PROJECT targetProject}
 * , {@link #PROPERTY_NAME_TARGET_PACKAGE targetPackage}.
 * 
 * @author lfg
 *
 */
public class ExtendModelAndExamplePlugin extends PluginAdapter {
	/** The property name of the target project. */
	private static final String PROPERTY_NAME_TARGET_PROJECT = "target.project";
	/** The property name of the target package. */
	private static final String PROPERTY_NAME_TARGET_PACKAGE = "target.package";
	/** Default encoding of java files. */
	private static final String DEFAULT_JAVA_FILE_ENCODING = "UTF-8";
	private static final String EXTEND_CLASS_PACKAGE = "ext";
	private static final String EXTEND_CLASS_SUFFIX = "Ext";
	private static final String EXTEND_MAPPER_XML_PACKAGE = "ext";
	private static final String EXTEND_MAPPER_XML_SUFFIX = "Ext";
	private static final String GENERIC_MAPPER_CLASS_NAME = GenericMapper.class.getName();
	private static final String GENERIC_MAPPER_CLASS_NAME_WITH_BLOB = GenericMapperWithBlobs.class.getName();
	private JavaFormatter javaFormatter = null;
	private XmlFormatter xmlFormatter = null;
	private String javaFileEncoding = null;

	private String javaModelTargetProject = null;
	private String javaModelTargetPackage = null;
	private String extJavaModelTargetPackage = null;

	private String recordPrimaryKeyClassName = null;
	private String recordClassName = null;
	private String recordShortClassName = null;
	private String extRecordClassName = null;
	private String extRecordShortClassName = null;
	private String recordClassNameWithBlob = null;
	private String recordShortClassNameWithBlob = null;

	private String exampleClassName = null;
	private String exampleShortClassName = null;
	private String extExampleClassName = null;
	private String extExampleShortClassName = null;

	private String mapperClassTargetProject = null;
	private String mapperClassTargetPackage = null;
	private String sqlMapperTargetProject = null;
	private String sqlMapperTargetPackage = null;
	private String mapperExtPackage = null;

	private String javaMapperClassName = null;
	private String javaMapperShortClassName = null;
	private String xmlMapperFileName = null;
	private String xmlMapperExtFileName = null;

	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		// context
		javaFormatter = context.getJavaFormatter();
		xmlFormatter = context.getXmlFormatter();
		javaFileEncoding = getJavaFileEncoding();
		System.out.println("");

		// javaModelGenerator
		JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = context.getJavaModelGeneratorConfiguration();
		javaModelTargetProject = javaModelGeneratorConfiguration.getTargetProject();
		javaModelTargetPackage = javaModelGeneratorConfiguration.getTargetPackage();
		extJavaModelTargetPackage = javaModelTargetPackage + "." + EXTEND_CLASS_PACKAGE;
		System.out.println("javaModelTargetProject -- " + javaModelTargetProject);
		System.out.println("javaModelTargetPackage -- " + javaModelTargetPackage);
		System.out.println("extJavaModelTargetPackage -- " + extJavaModelTargetPackage);
		System.out.println("");

		// properties
		// xml
		mapperClassTargetProject = properties.getProperty(PROPERTY_NAME_TARGET_PROJECT, javaModelTargetProject);
		mapperClassTargetPackage = properties.getProperty(PROPERTY_NAME_TARGET_PACKAGE, sqlMapperTargetPackage);
		System.out.println("mapperClassTargetProject -- " + mapperClassTargetProject);
		System.out.println("mapperClassTargetPackage -- " + mapperClassTargetPackage);
	}

	@Override
	public boolean validate(List<String> warnings) {
		return true;
	}

	@Override
	public void initialized(IntrospectedTable introspectedTable) {

		recordPrimaryKeyClassName = getPrimaryKeyType(introspectedTable);
		recordClassName = introspectedTable.getBaseRecordType();
		recordShortClassName = getShortClassName(recordClassName);
		recordClassNameWithBlob = recordClassName + "WithBLOBs";
		recordShortClassNameWithBlob = recordShortClassName + "WithBLOBs";
		exampleClassName = introspectedTable.getExampleType();
		exampleShortClassName = getShortClassName(exampleClassName);
		System.out.println("recordPrimaryKeyClassName -- " + recordPrimaryKeyClassName);
		System.out.println("recordClassName -- " + recordClassName);
		System.out.println("recordShortClassName -- " + recordShortClassName);
		System.out.println("recordClassNameWithBlob -- " + recordClassNameWithBlob);
		System.out.println("recordShortClassNameWithBlob -- " + recordShortClassNameWithBlob);
		System.out.println("exampleClassName -- " + exampleClassName);
		System.out.println("exampleShortClassName -- " + exampleShortClassName);
		System.out.println("");

		extRecordShortClassName = recordShortClassName + EXTEND_CLASS_SUFFIX;
		extRecordClassName = extJavaModelTargetPackage + "." + extRecordShortClassName;
		System.out.println("extRecordShortClassName -- " + extRecordShortClassName);
		System.out.println("extRecordClassName -- " + extRecordClassName);
		System.out.println("");

		extExampleShortClassName = exampleShortClassName + EXTEND_CLASS_SUFFIX;
		extExampleClassName = extJavaModelTargetPackage + "." + extExampleShortClassName;
		System.out.println("extExampleShortClassName -- " + extExampleShortClassName);
		System.out.println("extExampleClassName -- " + extExampleClassName);
		System.out.println("");

		SqlMapGeneratorConfiguration sqlMapGeneratorConfiguration = context.getSqlMapGeneratorConfiguration();
		sqlMapperTargetProject = sqlMapGeneratorConfiguration.getTargetProject();
		sqlMapperTargetPackage = sqlMapGeneratorConfiguration.getTargetPackage();
		mapperExtPackage = sqlMapperTargetPackage + "." + EXTEND_MAPPER_XML_PACKAGE;
		System.out.println("mapperTargetProject -- " + sqlMapperTargetProject);
		System.out.println("mapperTargetPackage -- " + sqlMapperTargetPackage);
		System.out.println("mapperExtPackage -- " + mapperExtPackage);
		System.out.println("");

		javaMapperShortClassName = recordShortClassName + "Mapper";
		javaMapperClassName = mapperClassTargetPackage + "." + javaMapperShortClassName;
		xmlMapperFileName = introspectedTable.getMyBatis3XmlMapperFileName();
		xmlMapperExtFileName = xmlMapperFileName.replaceAll("\\.xml$", EXTEND_MAPPER_XML_SUFFIX + ".xml");
		System.out.println("javaMapperClassName -- " + javaMapperClassName);
		System.out.println("javaMapperShortClassName -- " + javaMapperShortClassName);
		System.out.println("xmlMapperFileName -- " + xmlMapperFileName);
		System.out.println("xmlMapperExtFileName -- " + xmlMapperExtFileName);
		System.out.println("");

		super.initialized(introspectedTable);
	}

	@Override
	public boolean modelRecordWithBLOBsClassGenerated(TopLevelClass topLevelClass,
			IntrospectedTable introspectedTable) {
		topLevelClass.addImportedType(extRecordClassName);
		topLevelClass.setSuperClass(extRecordClassName);
		System.out.println("Change the super class of the " + topLevelClass.getType().getFullyQualifiedName() + " to "
				+ extRecordClassName);
		return super.modelRecordWithBLOBsClassGenerated(topLevelClass, introspectedTable);
	}

	@Override
	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {
		List<GeneratedJavaFile> extJavaFileList = new ArrayList<GeneratedJavaFile>();
		// extending class for base record
		GeneratedJavaFile extBaseRecordJavaFile = generateExtJavaFile(recordClassName, extRecordClassName,
				javaModelTargetProject, extJavaModelTargetPackage);
		if (!Objects.isNull(extBaseRecordJavaFile)) {
			extJavaFileList.add(extBaseRecordJavaFile);
		}

		// extending class for example
		GeneratedJavaFile extExampleJavaFile = generateExtJavaFile(exampleClassName, extExampleClassName,
				javaModelTargetProject, extJavaModelTargetPackage);
		if (!Objects.isNull(extExampleJavaFile)) {
			extJavaFileList.add(extExampleJavaFile);
		}

		// xml interface
		GeneratedJavaFile mapperJavaFile = generateMapperJavaFile(introspectedTable);
		if (!Objects.isNull(mapperJavaFile)) {
			extJavaFileList.add(mapperJavaFile);
		}

		return extJavaFileList;
	}

	/**
	 * @return
	 */
	private GeneratedJavaFile generateMapperJavaFile(IntrospectedTable introspectedTable) {
		Interface mapperJavaInterface = new Interface(javaMapperClassName);
		mapperJavaInterface.setVisibility(JavaVisibility.PUBLIC);
		// import
		mapperJavaInterface.addImportedType(new FullyQualifiedJavaType(recordPrimaryKeyClassName));
		mapperJavaInterface.addImportedType(new FullyQualifiedJavaType(extRecordClassName));
		mapperJavaInterface.addImportedType(new FullyQualifiedJavaType(extExampleClassName));
		if (isBlobClassExist(introspectedTable)) {
			// import
			mapperJavaInterface.addImportedType(new FullyQualifiedJavaType(recordClassNameWithBlob));
			mapperJavaInterface.addImportedType(new FullyQualifiedJavaType(GENERIC_MAPPER_CLASS_NAME_WITH_BLOB));

			// super class
			FullyQualifiedJavaType superInterface = new FullyQualifiedJavaType(GENERIC_MAPPER_CLASS_NAME_WITH_BLOB);
			superInterface.addTypeArgument(new FullyQualifiedJavaType(extRecordClassName));
			superInterface.addTypeArgument(new FullyQualifiedJavaType(recordClassNameWithBlob));
			superInterface.addTypeArgument(new FullyQualifiedJavaType(extExampleClassName));
			superInterface.addTypeArgument(new FullyQualifiedJavaType(recordPrimaryKeyClassName));
			mapperJavaInterface.addSuperInterface(superInterface);
		} else {
			// import
			mapperJavaInterface.addImportedType(new FullyQualifiedJavaType(GENERIC_MAPPER_CLASS_NAME));

			// super class
			FullyQualifiedJavaType superInterface = new FullyQualifiedJavaType(GENERIC_MAPPER_CLASS_NAME);
			superInterface.addTypeArgument(new FullyQualifiedJavaType(extRecordClassName));
			superInterface.addTypeArgument(new FullyQualifiedJavaType(extExampleClassName));
			superInterface.addTypeArgument(new FullyQualifiedJavaType(recordPrimaryKeyClassName));
			mapperJavaInterface.addSuperInterface(superInterface);
		}

		GeneratedJavaFile mapperJavaFile = new GeneratedJavaFile(mapperJavaInterface, mapperClassTargetProject,
				javaFileEncoding, javaFormatter);
		try {
			File targetFile = getTargetFile(mapperJavaFile.getTargetProject(), mapperJavaFile.getTargetPackage(),
					mapperJavaFile.getFileName());
			if (!targetFile.exists()) {
				System.out.println(
						"Add custom xml class. -- " + mapperJavaInterface.getType().getFullyQualifiedName());
				return mapperJavaFile;
			} else {
				System.out.println("Mapper class has existed. Don't override.");
			}
		} catch (ShellException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean sqlMapResultMapWithoutBLOBsElementGenerated(XmlElement element,
			IntrospectedTable introspectedTable) {
		replaceXmlElementAttribute(element, "type", extRecordClassName);
		return super.sqlMapResultMapWithoutBLOBsElementGenerated(element, introspectedTable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mybatis.generator.api.PluginAdapter#
	 * sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(org.mybatis.
	 * generator.api.dom.xml.XmlElement,
	 * org.mybatis.generator.api.IntrospectedTable)
	 */
	@Override
	public boolean sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(XmlElement element,
			IntrospectedTable introspectedTable) {
		replaceXmlElementAttribute(element, "parameterType", extRecordClassName);
		return super.sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(element, introspectedTable);
	}

	@Override
	public boolean sqlMapSelectByExampleWithBLOBsElementGenerated(XmlElement element,
			IntrospectedTable introspectedTable) {
		replaceXmlElementAttribute(element, "parameterType", extExampleClassName);
		return super.sqlMapSelectByExampleWithBLOBsElementGenerated(element, introspectedTable);
	}

	@Override
	public boolean sqlMapSelectByExampleWithoutBLOBsElementGenerated(XmlElement element,
			IntrospectedTable introspectedTable) {
		replaceXmlElementAttribute(element, "parameterType", extExampleClassName);
		return super.sqlMapSelectByExampleWithoutBLOBsElementGenerated(element, introspectedTable);
	}

	@Override
	public boolean sqlMapDeleteByExampleElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		replaceXmlElementAttribute(element, "parameterType", extExampleClassName);
		return super.sqlMapDeleteByExampleElementGenerated(element, introspectedTable);
	}

	@Override
	public boolean sqlMapCountByExampleElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		replaceXmlElementAttribute(element, "parameterType", extExampleClassName);
		return super.sqlMapCountByExampleElementGenerated(element, introspectedTable);
	}

	@Override
	public List<GeneratedXmlFile> contextGenerateAdditionalXmlFiles(IntrospectedTable introspectedTable) {
		List<GeneratedXmlFile> xmlFileList = new ArrayList<GeneratedXmlFile>();

		try {
			File targetFile = getTargetFile(sqlMapperTargetProject, mapperExtPackage, xmlMapperExtFileName);
			if (!targetFile.exists()) {
				Document extMapperDocument = new Document(XmlConstants.MYBATIS3_MAPPER_PUBLIC_ID,
						XmlConstants.MYBATIS3_MAPPER_SYSTEM_ID);

				XmlElement root = new XmlElement("xml");
				root.addAttribute(new Attribute("namespace", javaMapperClassName));
				root.addElement(new TextElement("<!--"));
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.append(" Generated time: ");
				strBuilder.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				strBuilder.append(".");
				root.addElement(new TextElement(strBuilder.toString()));
				root.addElement(new TextElement("Add extending methods here."));
				root.addElement(new TextElement("-->"));
				extMapperDocument.setRootElement(root);

				GeneratedXmlFile xmlFile = new GeneratedXmlFile(extMapperDocument, xmlMapperExtFileName,
						mapperExtPackage, sqlMapperTargetProject, false, xmlFormatter);
				xmlFileList.add(xmlFile);
				System.out.println(
						"Add extending xml xml file -- " + mapperExtPackage + File.separator + xmlMapperExtFileName);
			}
		} catch (ShellException e) {
			e.printStackTrace();
		}

		return xmlFileList;
	}

	/**
	 * Get the short name of the class name.
	 * 
	 * @param className
	 * @return
	 */
	private String getShortClassName(String className) {
		if (Objects.isNull(className)) {
			return null;
		}
		int lastIndex = className.lastIndexOf(".");
		if (lastIndex == -1) {
			return className;
		}
		return className.substring(lastIndex + 1);
	}

	/**
	 * Get the java file encoding.
	 * 
	 * @return
	 */
	private String getJavaFileEncoding() {
		String javaFileEncoding = context.getProperty("javaFileEncoding");

		javaFileEncoding = javaFileEncoding == null ? DEFAULT_JAVA_FILE_ENCODING : javaFileEncoding;

		return javaFileEncoding;
	}

	/**
	 * Get current timestamp.
	 * 
	 * @return
	 */
	private String getCurrentTimestamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new Date());
	}

	/**
	 * Generate the extending class for model classes.
	 * 
	 * @param baseClassName
	 * @param extClassName
	 * @param targetProject
	 * @param targetPackage
	 * @return
	 */
	private GeneratedJavaFile generateExtJavaFile(String baseClassName, String extClassName, String targetProject,
			String targetPackage) {
		TopLevelClass extClass = new TopLevelClass(extClassName);
		extClass.setVisibility(JavaVisibility.PUBLIC);
		extClass.addImportedType(baseClassName);
		extClass.setSuperClass(baseClassName);
		extClass.addFileCommentLine("/**");
		extClass.addFileCommentLine(" * Generate time:" + getCurrentTimestamp());
		extClass.addFileCommentLine(" */");
		extClass.addJavaDocLine("/**");
		extClass.addJavaDocLine(" * Add the extending methods here.");
		extClass.addJavaDocLine(" */");

		try {
			File classFile = getTargetFile(targetProject, targetPackage, extClass.getType().getShortName() + ".java");
			// Don't override if exits
			if (!classFile.exists()) {
				System.out.println("Add ext java record file. -- " + classFile.getName());
				GeneratedJavaFile extRecordFile = new GeneratedJavaFile(extClass, targetProject, javaFileEncoding,
						javaFormatter);
				return extRecordFile;
			} else {
				System.out.println(extClassName + " exists, don't override.");
			}
		} catch (ShellException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the File instance according to parameters.
	 * 
	 * @param targetProject
	 * @param targetPackage
	 * @param targetFileName
	 * @return
	 * @throws ShellException
	 */
	private File getTargetFile(String targetProject, String targetPackage, String targetFileName)
			throws ShellException {
		DefaultShellCallback shellCallback = new DefaultShellCallback(true);
		File directory = shellCallback.getDirectory(targetProject, targetPackage);
		return new File(directory, targetFileName);
	}

	/**
	 * Replace the attribute value.
	 * 
	 * @param elementId
	 * @param attributes
	 * @param attributeName
	 * @param targetAttributeName
	 */
	private void replaceXmlElementAttribute(XmlElement element, String attributeName, String targetAttributeName) {
		List<Attribute> attributes = element.getAttributes();
		String elementId = null;
		for (Attribute attribute : attributes) {
			if ("id".equalsIgnoreCase(attribute.getName())) {
				elementId = attribute.getValue();
				break;
			}
		}
		for (Attribute attribute : attributes) {
			if (attributeName.equalsIgnoreCase(attribute.getName())) {
				attributes.remove(attribute);
				attributes.add(new Attribute(attributeName, targetAttributeName));
				System.out.println("Change the attribute " + attributeName + " of " + elementId + " to "
						+ targetAttributeName + " from " + attribute.getValue());
				break;
			}
		}
	}

	/**
	 * Get the primary key type of the model record.
	 * 
	 * @param introspectedTable
	 * @return
	 */
	private String getPrimaryKeyType(IntrospectedTable introspectedTable) {
		ModelType modelType = introspectedTable.getTableConfiguration().getModelType();

		List<IntrospectedColumn> primaryKeyColumns = introspectedTable.getPrimaryKeyColumns();
		if (ModelType.FLAT.equals(modelType)
				|| (ModelType.CONDITIONAL.equals(modelType) || primaryKeyColumns.size() == 1)) {
			IntrospectedColumn introspectedColumn = primaryKeyColumns.get(0);
			return introspectedColumn.getFullyQualifiedJavaType().getFullyQualifiedName();
		} else {
			return introspectedTable.getPrimaryKeyType();
		}
	}

	/**
	 * Check whether the seperate blob model class exits.
	 * 
	 * @return true if exists, false otherwise.
	 */
	private boolean isBlobClassExist(IntrospectedTable introspectedTable) {
		ModelType modelType = introspectedTable.getTableConfiguration().getModelType();
		List<IntrospectedColumn> blobColumns = introspectedTable.getBLOBColumns();
		if (ModelType.FLAT.equals(modelType) || (ModelType.CONDITIONAL.equals(modelType) && blobColumns.size() == 0)) {
			return false;
		}

		return true;
	}
}
