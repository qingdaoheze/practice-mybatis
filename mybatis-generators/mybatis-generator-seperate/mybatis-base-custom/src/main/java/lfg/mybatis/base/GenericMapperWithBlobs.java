/**
 *@date 2017年11月13日 上午9:05:24 
 */
package lfg.mybatis.base;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 *
 * T  表实体 <br/>
 * TB 含有Blob字段的表实体
 * TE example实体 <br/>
 * PK 主键实体 <br/>
 * @author lfg
 *
 */
public interface GenericMapperWithBlobs <T, TB, TE, PK> {
    long countByExample(TE example);

    int deleteByExample(TE example);

    int deleteByPrimaryKey(PK id);

    int insert(TB record);

    int insertSelective(TB record);

    List<T> selectByExample(TE example);

    List<TB> selectByExampleWithBLOBs(TE example);

    T selectByPrimaryKey(PK id);

    int updateByExampleSelective(@Param("record") T record, @Param("example") TE example);

    int updateByExample(T record, TE example);

    int updateByPrimaryKeySelective(TB record);

    int updateByPrimaryKey(T record);

}