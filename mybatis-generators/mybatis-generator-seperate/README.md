# 使用mybatis-generator插件完成通用代码的生成
*** 使用这种方式生成代码，是以对象的方式构造文件，然后输出。 ***

通过mybatis-generator插件实现mybatis代码的生成和定制、以及其他通用性代码生成。

* mybatis-base-custom包含了定制的插件、插件中使用到且在生成的代码中依赖的通用类。
* mybatis-generator-custom是代码生成项目。