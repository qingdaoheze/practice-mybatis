# practice-mybatis

## 代码说明

### mybatis使用

位于包：

```java
lfg.practice.mybatis
```

测试入口类：

`lfg.practice.mybatis.GeneralOperations`

### 手写实现

位于包：

```java
lfg.practice.framework
```

测试入口类：

`lfg.practice.framework.mybatis.v1.Entry`

# 实施步骤

1. 导入mybatis和数据库驱动的maven依赖
2. mybatis配置文件（/practice-mybatis/src/main/resources/mybatis/mybatis-config.xml）
3. mybatis的mapper文件
   - /practice-mybatis/src/main/resources/mybatis/mapper/BlogMapper.xml
   - /practice-mybatis/src/main/resources/lfg/practice/mybatis/mapper/BlogMapper.xml
4. mapper接口
/practice-mybatis/src/main/java/lfg/practice/mybatis/mapper/BlogMapper.java
5. 编写测试代码
lfg.practice.mybatis.ApplicationEntry

## 外部属性文件
同名属性值的优先级为：代码中以参数传入的参数 > 外部属性文件中的参数 > properties子元素定义的参数. 它们都会加载，只是会被高优先级的值覆盖。

## Mapper的加载机制
1. 如果只配置xml格式的mapper文件，namespace对应的mapper类会自动被扫描和加载。
2. 如果只配置mapper类，则其相同目录下的同名xml文件也会被加载。
3. xml和class可以同时配置，但相同id的操作只能定义一次，不能同时包含。

## page-helper用法
[github](https://gitee.com/free/Mybatis_PageHelper)
[码云](https://github.com/pagehelper/Mybatis-PageHelper)
1. 添加依赖
```xml

        <dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper</artifactId>
            <version>5.1.4</version>
        </dependency>
```

2. 在配置文件中添加插件
```xml

    <plugins>
        <!-- com.github.pagehelper为PageHelper类所在包名 -->
        <plugin interceptor="com.github.pagehelper.PageInterceptor">
            <!-- 使用下面的方式配置参数，后面会有所有的参数介绍 -->
            <property name="helperDialect" value="mysql" />
            <property name="reasonable" value="true" />
            <property name="pageSizeZero" value="true" />
        </plugin>
    </plugins>
```
3. 在查询语句的前面插入pageHelperd代码
```java

    PageHelper.startPage(2, 3);
    List<Blog> selectBlog = mapper.selectBlog(0);
```

