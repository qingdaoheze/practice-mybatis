package lfg.practice.mybatis.enums;

import lombok.Getter;

@Getter
public enum BlogType {
	RED(0), BLUE(1), GREEN(2), YELLOW(3);
	private int id;
	private BlogType(int id) {
		this.id = id;
	}
}
