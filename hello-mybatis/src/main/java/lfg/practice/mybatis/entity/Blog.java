package lfg.practice.mybatis.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Blog implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String title;
	private String content;
	private int type;
}
