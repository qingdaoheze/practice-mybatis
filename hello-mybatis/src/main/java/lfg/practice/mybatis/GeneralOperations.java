package lfg.practice.mybatis;

import java.io.IOException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.github.pagehelper.PageHelper;

import lfg.practice.mybatis.entity.Blog;
import lfg.practice.mybatis.mapper.BlogMapper;

/**
 * 常规的增删改查
 * 
 * @author lifenggang
 *
 */
public class GeneralOperations {

	public static void main(String[] args) throws IOException {
		// openSession()默认是开启事务的
		try (SqlSession session = ConnectionManager.getSession()) {
			BlogMapper mapper = session.getMapper(BlogMapper.class);
			long targetId = 2;
			// insert
			for (int i = 0; i < 8; i++) {
				Blog blog = new Blog();
				blog.setTitle("blog" + i);
				blog.setContent("content" + i);
				blog.setType(i);
				int rows = mapper.insert(blog);
				System.out.println(rows == 1 ? "Insert success:" + blog.getId() : "Insert fail");
				targetId = blog.getId();
			}
			// selectById
			Blog selectById = mapper.selectById(targetId);
			System.out.println("selectById: " + selectById.toString());
	
			System.out.println();
	
			selectById.setContent("Content is modified.");
			// update
			int updateRows = mapper.update(selectById);
			System.out.println(updateRows == 1 ? "update success" : "udpate fail");
			System.out.println("update:" + selectById.toString());
	
			System.out.println();
			// selectBlog with pagehelper
			PageHelper.startPage(2, 4);
			List<Blog> selectBlog = mapper.selectBlog(0);
			for (Blog blog : selectBlog) {
				System.out.println("selectBlog:" + blog.toString());
			}
			session.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
