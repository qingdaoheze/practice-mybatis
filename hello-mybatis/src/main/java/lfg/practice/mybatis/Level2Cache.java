package lfg.practice.mybatis;

import java.sql.ResultSet;

import lfg.demo.utils.Closer;
import org.apache.ibatis.session.SqlSession;

import lfg.practice.mybatis.entity.Blog;
import lfg.practice.mybatis.mapper.BlogMapper;

public class Level2Cache {

	public static void main(String[] args) {
//		onlySelect();
//		flushCache();
		nocacheStateInMapper();
//		selectWithUpdate();
	}

	/**
	 * 关闭会话或者更新操作，才会导致二级缓存生效。
	 */
	static void onlySelect() {

		SqlSession session0 = null;
		SqlSession session1 = null;
		SqlSession session2 = null;
		SqlSession session3 = null;
		try {
			session0 = ConnectionManager.getSession();
			session1 = ConnectionManager.getSession();
			session2 = ConnectionManager.getSession();
			session3 = ConnectionManager.getSession();
			BlogMapper mapper0 = session0.getMapper(BlogMapper.class);
			BlogMapper mapper1 = session1.getMapper(BlogMapper.class);
			BlogMapper mapper2 = session2.getMapper(BlogMapper.class);
			BlogMapper mapper3 = session3.getMapper(BlogMapper.class);

			ResultSet rs = session0.getConnection().prepareStatement("select @@tx_isolation").executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString(1));
			}
			Blog blog0 = new Blog();
			blog0.setTitle("level2");
			blog0.setContent("level2 content");
			blog0.setType(2);

			mapper0.insert(blog0);
			session0.commit();
			Closer.closeSilently(session0);
			System.out.println("blog0: " + blog0.toString());
			System.out.println("*************************** blog0 ****************");

			// select 1
			long id = blog0.getId();
			long start = System.currentTimeMillis();
			Blog blog1 = mapper1.selectById(id);
			long elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog1: " + elapseTime + ":" + blog1.toString());
//			Closer.closeSilently(session1);
			System.out.println("*************************** blog1 ****************");

			// select 2
			start = System.currentTimeMillis();
			Blog blog2 = mapper2.selectById(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog2: " + elapseTime + ":" + blog2.toString());
			Closer.closeSilently(session2);
			System.out.println("*************************** blog2 ****************");

			// select 3
			start = System.currentTimeMillis();
			Blog blog3 = mapper3.selectById(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog3:" + elapseTime + ":" + blog3.toString());
			Closer.closeSilently(session3);
			System.out.println("*************************** blog3 ****************");

			// remove test record
			session0 = ConnectionManager.getSession();
			mapper0 = session0.getMapper(BlogMapper.class);
			mapper0.delete(id);
			session0.commit();
			System.out.println("*************************** remove blog0 ****************");

			System.out.println("End");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Closer.closeSilently(session3);
			Closer.closeSilently(session2);
			Closer.closeSilently(session1);
			Closer.closeSilently(session0);
		}
	}

	/**
	 * 如果select上带有flushCahce选项，那么会在缓存命中后，仍然执行查询操作。
	 */
	static void flushCache() {

		SqlSession session0 = null;
		SqlSession session1 = null;
		SqlSession session2 = null;
		SqlSession session3 = null;
		try {
			session0 = ConnectionManager.getSession();
			session1 = ConnectionManager.getSession();
			session2 = ConnectionManager.getSession();
			session3 = ConnectionManager.getSession();
			BlogMapper mapper0 = session0.getMapper(BlogMapper.class);
			BlogMapper mapper1 = session1.getMapper(BlogMapper.class);
			BlogMapper mapper2 = session2.getMapper(BlogMapper.class);
			BlogMapper mapper3 = session3.getMapper(BlogMapper.class);

			ResultSet rs = session0.getConnection().prepareStatement("select @@tx_isolation").executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString(1));
			}
			Blog blog0 = new Blog();
			blog0.setTitle("level2");
			blog0.setContent("level2 content");
			blog0.setType(2);

			mapper0.insert(blog0);
			session0.commit();
			Closer.closeSilently(session0);
			System.out.println("blog0: " + blog0.toString());
			System.out.println("*************************** blog0 ****************");

			// select 1
			long id = blog0.getId();
			long start = System.currentTimeMillis();
			Blog blog1 = mapper1.selectByIdWithoutCache(id);
			long elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog1: " + elapseTime + ":" + blog1.toString());
//			Closer.closeSilently(session1);
			System.out.println("*************************** blog1 ****************");

			// select 2
			start = System.currentTimeMillis();
			Blog blog2 = mapper2.selectByIdWithoutCache(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog2: " + elapseTime + ":" + blog2.toString());
			Closer.closeSilently(session2);
			System.out.println("*************************** blog2 ****************");

			// select 3
			start = System.currentTimeMillis();
			Blog blog3 = mapper3.selectByIdWithoutCache(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog3:" + elapseTime + ":" + blog3.toString());
			Closer.closeSilently(session3);
			System.out.println("*************************** blog3 ****************");

			// remove test record
			session0 = ConnectionManager.getSession();
			mapper0 = session0.getMapper(BlogMapper.class);
			mapper0.delete(id);
			session0.commit();
			System.out.println("*************************** remove blog0 ****************");

			System.out.println("End");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Closer.closeSilently(session3);
			Closer.closeSilently(session2);
			Closer.closeSilently(session1);
			Closer.closeSilently(session0);
		}
	}

	/**
	 * mapper配置文件中未开启缓存
	 */
	static void nocacheStateInMapper() {

		SqlSession session0 = null;
		SqlSession session1 = null;
		SqlSession session2 = null;
		SqlSession session3 = null;
		try {
			session0 = ConnectionManager.getSession();
			session1 = ConnectionManager.getSession();
			session2 = ConnectionManager.getSession();
			session3 = ConnectionManager.getSession();
			BlogMapper mapper0 = session0.getMapper(BlogMapper.class);
			BlogMapper mapper1 = session1.getMapper(BlogMapper.class);
			BlogMapper mapper2 = session2.getMapper(BlogMapper.class);
			BlogMapper mapper3 = session3.getMapper(BlogMapper.class);

			ResultSet rs = session0.getConnection().prepareStatement("select @@tx_isolation").executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString(1));
			}
			Blog blog0 = new Blog();
			blog0.setTitle("level2");
			blog0.setContent("level2 content");
			blog0.setType(2);

			mapper0.insert(blog0);
			session0.commit();
			Closer.closeSilently(session0);
			System.out.println("blog0: " + blog0.toString());
			System.out.println("*************************** blog0 ****************");

			// select 1
			long id = blog0.getId();
			long start = System.currentTimeMillis();
			Blog blog1 = mapper1.selectByIdInMapper(id);
			long elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog1: " + elapseTime + ":" + blog1.toString());
//			Closer.closeSilently(session1);
			System.out.println("*************************** blog1 ****************");

			// select 2
			start = System.currentTimeMillis();
			Blog blog2 = mapper2.selectByIdInMapper(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog2: " + elapseTime + ":" + blog2.toString());
			Closer.closeSilently(session2);
			System.out.println("*************************** blog2 ****************");

			// select 3
			start = System.currentTimeMillis();
			Blog blog3 = mapper3.selectByIdInMapper(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog3:" + elapseTime + ":" + blog3.toString());
			Closer.closeSilently(session3);
			System.out.println("*************************** blog3 ****************");

			// remove test record
			session0 = ConnectionManager.getSession();
			mapper0 = session0.getMapper(BlogMapper.class);
			mapper0.delete(id);
			session0.commit();
			System.out.println("*************************** remove blog0 ****************");

			System.out.println("End");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Closer.closeSilently(session3);
			Closer.closeSilently(session2);
			Closer.closeSilently(session1);
			Closer.closeSilently(session0);
		}
	}
	
	static void selectWithUpdate() {

		SqlSession session0 = null;
		SqlSession session1 = null;
		SqlSession session2 = null;
		try {
			session0 = ConnectionManager.getSession();
			session1 = ConnectionManager.getSession();
			session2 = ConnectionManager.getSession();
			BlogMapper mapper0 = session0.getMapper(BlogMapper.class);
			BlogMapper mapper1 = session1.getMapper(BlogMapper.class);
			BlogMapper mapper2 = session2.getMapper(BlogMapper.class);

			ResultSet rs = session0.getConnection().prepareStatement("select @@tx_isolation").executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString(1));
			}
			Blog blog0 = new Blog();
			blog0.setTitle("level2");
			blog0.setContent("level2 content");
			blog0.setType(2);

			mapper0.insert(blog0);
			session0.commit();
			Closer.closeSilently(session0);
			System.out.println("blog0: " + blog0.toString());

			long id = blog0.getId();
			long start = System.currentTimeMillis();
			Blog blog1 = mapper1.selectById(id);
			long elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog1: " + elapseTime + ":" + blog1.toString());

			start = System.currentTimeMillis();
			Blog blog2 = mapper2.selectById(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog2: " + elapseTime + ":" + blog2.toString());
			blog2.setTitle("level2 -- >");
			mapper2.update(blog2);
			session2.commit();
			start = System.currentTimeMillis();
			Blog blog2_1 = mapper2.selectById(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog2_1:" + elapseTime + ":" + blog2_1.toString());

			start = System.currentTimeMillis();
			Blog blog1_1 = mapper1.selectById(id);
			elapseTime = System.currentTimeMillis() - start;
			System.out.println("blog1_1:" + elapseTime + ":" + blog1_1.toString());

			mapper1.delete(id);
			session1.commit();
			session2.rollback();

			System.out.println("End");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			Closer.closeSilently(session2);
			Closer.closeSilently(session1);
			Closer.closeSilently(session0);
		}
	}

}
