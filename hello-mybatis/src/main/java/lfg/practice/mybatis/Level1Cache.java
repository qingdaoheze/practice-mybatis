package lfg.practice.mybatis;

import java.io.IOException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import lfg.practice.mybatis.entity.Blog;
import lfg.practice.mybatis.mapper.BlogMapper;

public class Level1Cache {

	public static void main(String[] args) throws IOException {
		// openSession()默认是开启事务的
		try (SqlSession session = ConnectionManager.getSession();) {
			BlogMapper mapper = session.getMapper(BlogMapper.class);
			long targetId = 2;
			// insert
			for (int i = 0; i < 1; i++) {
				Blog blog = new Blog();
				blog.setTitle("blog" + i);
				blog.setContent("content" + i);
				blog.setType(i);
				int rows = mapper.insert(blog);
				System.out.println(rows == 1 ? "Insert success:" + blog.getId() : "Insert fail");
				targetId = blog.getId();
			}
			// selectById
			Blog selectById = mapper.selectById(targetId);
			System.out.println(selectById.toString());
	
			// selectById
			Blog blog2 = mapper.selectById(targetId);
			System.out.println( blog2.toString());
			System.out.println("************** 使用了1级缓存");

			// selectById
			Blog blog3 = mapper.selectByIdWithoutCache(targetId);
			System.out.println(blog3.toString());
	
			// selectById
			Blog blog4 = mapper.selectByIdWithoutCache(targetId);
			System.out.println( blog4.toString());
			System.out.println("************** 未使用1级缓存");

			List<Blog> selectBlog = mapper.selectBlog(0);
			for (Blog blog : selectBlog) {
				System.out.println("selectBlog:" + blog.toString());
			}
			// remove cache
			mapper.selectByIdWithoutCache(targetId);
			selectBlog = mapper.selectBlog(0);
			for (Blog blog : selectBlog) {
				System.out.println("selectBlog:" + blog.toString());
			}
			System.out.println("**************  是否使用1级缓存取决于中间是否有清除缓存的语句");

			session.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
