package lfg.practice.mybatis;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class ConnectionManager {
	private static final SqlSessionFactory sqlSessionFactory;
	
	static {
		String resource = "mybatis/mybatis-config.xml";
		try (InputStream inputStream = Resources.getResourceAsStream(resource);) {
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final SqlSession getSession() {
		return sqlSessionFactory.openSession();
//		return sqlSessionFactory.openSession(TransactionIsolationLevel.READ_COMMITTED);
	}
}
