package lfg.practice.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Options.FlushCachePolicy;

import lfg.practice.mybatis.entity.Blog;

@CacheNamespace()
public interface BlogMapper {
	@Select("select * from blog where id=#{id1}")
	@Options(useCache=true)
	Blog selectById(long id);

	@Select("select * from blog where id=#{id1}")
	@Options(flushCache=FlushCachePolicy.TRUE)
	Blog selectByIdWithoutCache(long id);

	Blog selectByIdInMapper(long id);

	/** 选择id大于参数的blog */
	List<Blog> selectBlog(long id);
	
	int insert(Blog blog);

	int update(Blog blog);
	
	int delete(long id);
}
