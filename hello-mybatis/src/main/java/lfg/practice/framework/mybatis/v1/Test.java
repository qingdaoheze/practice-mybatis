package lfg.practice.framework.mybatis.v1;

import lombok.Data;

@Data
public class Test {
	private int id;
	private int nums;
	private String name;
}
