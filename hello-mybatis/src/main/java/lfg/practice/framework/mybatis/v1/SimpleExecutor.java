package lfg.practice.framework.mybatis.v1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SimpleExecutor implements Executor {

	@Override
	public <T> T query(String statement, String parameter) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/java_demo?userUnicode=true&characterEncoding=utf-8&useSSL=false",
					"root", "123456");
			PreparedStatement pstmt = (PreparedStatement) conn.prepareStatement(statement);
			pstmt.setInt(1, Integer.parseInt(parameter));
			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				Test test = new Test();
				test.setId(resultSet.getInt("id"));
				test.setNums(resultSet.getInt("nums"));
				test.setName(resultSet.getString("name"));
				return (T) test;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
