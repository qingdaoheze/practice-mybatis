package lfg.practice.framework.mybatis.v1;

public interface TestMapper {
    Test selectByPrimaryKey(Integer id);
}