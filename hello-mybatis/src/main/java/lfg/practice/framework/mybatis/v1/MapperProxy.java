package lfg.practice.framework.mybatis.v1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MapperProxy implements InvocationHandler {
	private SqlSession sqlSession;

	public MapperProxy(SqlSession sqlSession) {
		super();
		this.sqlSession = sqlSession;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (Configuration.TestMapperXml.namespace.equals(method.getDeclaringClass().getName())) {
			String sql = Configuration.TestMapperXml.methodSqlMapping.get(method.getName());
			return sqlSession.selectOne(sql, String.valueOf(args[0]));
		}
		return null;
	}

}
