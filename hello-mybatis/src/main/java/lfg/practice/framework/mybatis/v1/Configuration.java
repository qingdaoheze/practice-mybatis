package lfg.practice.framework.mybatis.v1;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class Configuration {

	@SuppressWarnings("unchecked")
	public <T> T getMapper(Class<T> clazz, SqlSession sqlSession) {
		MapperProxy mapperProxy = new MapperProxy(sqlSession);
		return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz }, mapperProxy);
	}

	static class TestMapperXml {
		public static final String namespace = "lfg.practice.framework.mybatis.v1.TestMapper";
		public static final Map<String, String> methodSqlMapping = new HashMap<String, String>();

		static {
			methodSqlMapping.put("selectByPrimaryKey", "select * from test where id=?");
		}
	}
}
