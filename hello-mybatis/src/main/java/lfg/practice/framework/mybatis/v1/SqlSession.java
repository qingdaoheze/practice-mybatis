package lfg.practice.framework.mybatis.v1;

public class SqlSession {
	Configuration configuration;
	Executor executor;

	public SqlSession(Configuration configuration, Executor executor) {
		super();
		this.configuration = configuration;
		this.executor = executor;
	}

	public <T> T getMapper(Class<T> clazz) {
		return configuration.getMapper(clazz, this);
	}
	
	public <T> T selectOne(String statement, String parameter) {
		return executor.query(statement, parameter);
	}
}
