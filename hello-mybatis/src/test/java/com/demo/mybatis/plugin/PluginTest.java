package com.demo.mybatis.plugin;

import org.apache.ibatis.plugin.*;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 测试。
 * 所有类型的插件使用的都是JDK的动态代理，根据注解中定义的被拦截方法，进行相关的拦截。
 *
 * @author qd-hz
 */
public class PluginTest {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        ExamplePlugin examplePlugin = new ExamplePlugin();

        Properties properties = new Properties();
        properties.put("a", "b");
        examplePlugin.setProperties(properties);
        map = (Map)examplePlugin.plugin(map);
        System.out.println("map.get=" + map.get(""));
    }


    @Intercepts({@Signature(type=Map.class, method="get", args = {Object.class})})
    private static class ExamplePlugin implements Interceptor {

        @Override
        public Object intercept(Invocation invocation) throws Throwable {
            return "ExamplePlugin";
        }

        @Override
        public Object plugin(Object target) {
            return Plugin.wrap(target, this);
        }

        @Override
        public void setProperties(Properties properties) {
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String propertyName = (String)propertyNames.nextElement();
                System.out.println(propertyName + "=" + properties.getProperty(propertyName));
            }
        }
    }
}
